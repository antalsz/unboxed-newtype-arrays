# unboxed-newtype-arrays

The array package provides unboxed arrays only of specific types, and makes
creating new instances a major pain.  The `UnboxedNewtypeArrays` module
demonstrates one technique for relatively-safely creating wrappers.  This module
is currently best read as source code (for links and such, use the Haddock
representation); I should probably write it up as a literate Haskell file, but
we'll see if that ever happens.

For now, to best read this, I recommend
```
git clone git@gitlab.com:antalsz/unboxed-newtype-arrays.git
cd unboxed-newtype-arrays
cabal build
```
That should do some work and finally print out `Documentation created:` followed
by a path to an HTML file (and other files); open the HTML file.  If it doesn't,
print those out, try running `cabal haddock`.  That way, you can read the
formatted text and/or the prettified source.

A perfectly good second-best option is just to read `UnboxedNewtypeArrays.hs`
directly, either in the browser or in your editor of choice.
