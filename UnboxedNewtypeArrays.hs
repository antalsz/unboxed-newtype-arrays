{-|
Module      : UnboxedNewtypeArrays
Description : Unboxed arrays of newtype-wrapped values
Copyright   : © Antal Spector-Zabusky, 2020
License     : Apache-2.0
Maintainer  : antal.b.sz@gmail.com
Stability   : example code
Portability : GHC internals

This module demonstrates a technique for creating unboxed mutable and immutable
arrays of newtype-wrapped values, mostly safely.  Packaging this up in a
sufficiently-configurable way is enough of a pain that I am simply presenting
this as an example of a technique, although copying and adapting the instances
at the bottom of this file should work fine.  /NB/: There may be situations in
which this technique is unsafe; see "The caveat", below.  However, I'm unclear
on what those are.  I'm hopeful that they're the sort of situations where, if
you'd have problems, you'll already be aware (and if we're really lucky, there
aren't any such situations anyway).

__The goal:__ We'd like to have unboxed arrays of custom types, particularly
newtypes over currently-unboxable types.

__The problem:__ The unboxed array types 'UArray', 'STUArray', and 'IOUArray'
are nice enough, but they're very limited, in two ways:

1. First, they can only be used with the specific set of types the developers of
   the @array@ package listed, and adding new instances through the
   functionality in @Data.Array.Base@ isn't well-documented (indeed, that module
   doesn't even have a Haddock page) and requires unsafe tinkering.

2. Second, even if we're dealing with a newtype, 'UArray', 'STUArray', and
   'IOUArray' are all /nominal/ in their elements instead of representational,
   so we cannot simply 'coerce' between the two.  While we happen to know that
   'unsafeCoerce' would be fine here, it's too easy to make a mistake.

__The solution:__ If we could solve problem 2 by providing a custom coercion, we
would be set; the solution would be to define instances of 'IArray' and 'MArray'
that use the custom coercion instead of 'coerce' or 'unsafeCoerce'.  Thus, the
core of our solution is exactly that: We write a custom type '<-->', where @a
'<-->' a'@ is effectively "@'Coercible' a a'@ if you could pretend that
'UArray', 'STUArray', and 'IOUArray' were representational in ther elements."
We then write a function 'coerceArr' which applies one of these coercions in the
forwards direction.  Finally, knowing that this function will never actually do
any work, we provide a rewrite rule replacing @'coerceArr' c@ with @c `'seq'`
'unsafeCoerce'@.

Now that we have '<-->', we can apply it by writing instances like the following:

@
instance 'MArray' ('STUArray' s) e ('ST' s) => 'MArray' ('STUArray' s) ('Wrapped' e) ('ST' s) where
  'getBounds' = 'coerceArr' ('STArr' @e 'Co' ':->' 'LiftST' 'Id') 'getBounds'
  -- ... and so on ...
  {\-\# INLINE 'getBounds' #-\}
  -- ... and so on ...
@

As you can see, the downside to this approach is that we have to write out our
'<-->' explicitly to represent the form of the type we're coercing (for more
details, see the documentation for '<-->').  However, the upside is that we get
safe instances of 'IArray' and 'MArray' for unboxed arrays of our newtype!

__The caveat:__ The roles of 'UArray', 'STUArray', and 'MArray' are deliberately set to
nominal instead of representational.  We should probably listen, right?  Well, I
claim it depends.  Certainly, for simple cases like 'Wrapped', there's no need
to worry.  But in general, there was discussion about this in
<https://gitlab.haskell.org/ghc/ghc/-/issues/9220 GHC issue #9220, "type roles for unboxed arrays">,
and the roles were carefully discussed.  But it's not clear to me why
representational is wrong for /unboxed/ arrays, not just for
/'Foreign.Storable.Storable'/ arrays.  So please, if you're using this, exercise
caution if you think your types are special and shouldn't have arrays that are
identical to their representation's arrays.  But I'm not sure when that would be
the case – I'd welcome clarity on this.
-}
{-# OPTIONS_HADDOCK show-extensions #-}

{-# LANGUAGE
    FlexibleContexts
  , GADTs
  , MultiParamTypeClasses
  , ScopedTypeVariables
  , TypeApplications
  , TypeOperators #-}

module UnboxedNewtypeArrays (
  -- * The example newtype with instances
  type Wrapped(..),
  -- * The type of representational-ish coercions
  type (<-->)(..),
  -- * Applying representational-ish coercions
  coerceArr, coerceArr'
) where

import Data.Coerce
import Unsafe.Coerce

import Control.Monad.ST

import Data.Array.Base
import Data.Array.IO.Internals

-- |The type of representational-ish coercions: a value of type @a <--> a'@ is
-- like having a @'Coercible' a a'@ constraint that also treats 'STUArray' and
-- 'IOUArray' representationally in their final arguments.  Because this type is
-- strict in all its fields, simply 'seq'ing on it is enough to confirm that @a@
-- and @a'@ are representationally-ish equivalent.  In that case,
-- /if your @a@ and @a'@ are well-enough behaved/, it is safe to
-- @'unsafeCoerce' \@a \@a'@ from @a@ to @a'@; this is wrapped up in 'coerceArr'
-- and its associated rewrite rule.  The "well-enough behaved" requirement is
-- roughly that the types have the same representations and not e.g. have
-- different 'Foreign.Storable.Storable' instances; however, it's not clear to
-- me why that should matter here.  If it's clear to you, then you probably know
-- to avoid coercing anyway, hopefully?  See
-- <https://gitlab.haskell.org/ghc/ghc/-/issues/9220 GHC issue #9220, "type roles for unboxed arrays">
-- for more information.
--
-- To use this type, we construct values that correspond to the type; for
-- example, if we're converting
--
-- @
-- getBounds :: STUArray s i e -> m (i,i)
-- @
--
-- to work for 'STUArray's of @'Wrapped' e@s, then we write
--
-- @
-- coerceArr (STArr @e Co :-> LiftST Id) getBounds
-- @
--
-- The @STArr \@e Co@ case says that we're handling an 'STUArray' as the first
-- argument to this function, and the 'STUArray'\'s elements should be coerced.
-- The @LiftST Id@ is similar, but instead says we're lifting through @'ST' s@,
-- and the types are identical instead of being 'Coercible'.  (We could use 'Co'
-- as well, since identical types are coercible, but 'Id' produces better type
-- inference, which is important for the polymorphism in 'unsafeAccum' and
-- 'unsafeAccumArray' in 'IArray'.)
data a <--> a' where
  -- |The two types are already the same; no further work is needed
  Id :: a <--> a
  -- |The two types are already 'Coercible'; no further work is needed
  Co :: Coercible a a' => a <--> a'
  -- |Lift one of our coercions to the elements of a 'UArray'
  UArr :: !(a <--> a') -> (UArray i a <--> UArray i a')
  -- |Lift one of our coercions to the elements of an 'STUArray'
  STArr :: !(a <--> a') -> (STUArray s i a <--> STUArray s i a')
  -- |Lift one of our coercions to the elements of an 'IOUArray'
  IOArr :: !(a <--> a') -> (IOUArray i a <--> IOUArray i a')
  -- |Lift one of our coercions through @'ST' s@
  LiftST :: !(a <--> a') -> (ST s a <--> ST s a')
  -- |Lift one of our coercions through 'IO'
  LiftIO :: !(a <--> a') -> (IO a <--> IO a')
  -- |Lift one of our coercions through a function type
  (:->) :: !(a <--> a') -> !(b <--> b') -> ((a -> b) <--> (a' -> b'))
infix  0 <-->
infixr 9 :->

-- |Apply a representational-ish coercion.  This is usually how you want to use
-- one of these coercions; it will be replaced with 'unsafeCoerce' via a rewrite
-- rule.  The existence of this function, and the fact that it type-checks,
-- assure us that the rewrite rule is safe.  This function is safe-ish modulo
-- the assumptions about newtypes mentioned above; even without rewrite rules,
-- it already unsafely reinterprets the contents of arrays.
coerceArr :: (a <--> a') -> a -> a'
coerceArr Id         = id
coerceArr Co         = coerce
coerceArr (UArr  _)  = \(UArray l u n a) -> UArray l u n a
coerceArr (STArr _)  = \(STUArray l u n a) -> STUArray l u n a
coerceArr (IOArr a)  = coerce $ coerceArr (STArr a)
coerceArr (LiftST a) = fmap $ coerceArr a
coerceArr (LiftIO a) = fmap $ coerceArr a
coerceArr (a :-> b)  = \f -> coerceArr b . f . coerceArr' a
{-# NOINLINE coerceArr #-}

-- |Apply a representational-ish coercion backwards.  Used in the implementation
-- of 'coerceArr'; probably not otherwise necessary.
coerceArr' :: (a <--> a') -> a' -> a
coerceArr' Id         = id
coerceArr' Co         = coerce
coerceArr' (UArr  _)  = \(UArray l u n a) -> UArray l u n a
coerceArr' (STArr _)  = \(STUArray l u n a) -> STUArray l u n a
coerceArr' (IOArr a)  = coerce $ coerceArr' (STArr a)
coerceArr' (LiftST a) = fmap $ coerceArr' a
coerceArr' (LiftIO a) = fmap $ coerceArr' a
coerceArr' (a :-> b)  = \f -> coerceArr' b . f . coerceArr a
{-# NOINLINE coerceArr' #-}

{-# RULES
"coerceArr->unsafeCoerce"  forall c. coerceArr  c = c `seq` unsafeCoerce
"coerceArr'->unsafeCoerce" forall c. coerceArr' c = c `seq` unsafeCoerce
#-}

-- |A simple newtype for demonstrating the technique
newtype Wrapped a = Wrapped { unwrap :: a }

-- |Unboxed immutable 'UArray's of 'Wrapped' values.
instance IArray UArray e => IArray UArray (Wrapped e) where
  bounds           = coerceArr (UArr @e Co         :->                              Co)         bounds
  numElements      = coerceArr (UArr @e Co         :->                              Co)         numElements
  unsafeArray      = coerceArr (Id                 :-> Co         :->               UArr @e Co) unsafeArray
  unsafeAt         = coerceArr (UArr @e Co         :-> Id         :->               Co)         unsafeAt
  unsafeReplace    = coerceArr (UArr @e Co         :-> Co         :->               UArr @e Co) unsafeReplace
  unsafeAccum      = coerceArr ((Co :-> Id :-> Co) :-> UArr @e Co :-> Co :->        UArr @e Co) unsafeAccum
  unsafeAccumArray = coerceArr ((Co :-> Id :-> Co) :-> Co         :-> Id :-> Co :-> UArr @e Co) unsafeAccumArray

  {-# INLINE bounds           #-}
  {-# INLINE numElements      #-}
  {-# INLINE unsafeArray      #-}
  {-# INLINE unsafeAt         #-}
  {-# INLINE unsafeReplace    #-}
  {-# INLINE unsafeAccum      #-}
  {-# INLINE unsafeAccumArray #-}

-- |Unboxed mutable 'STUArray's of 'Wrapped' values.  This instance is the same
-- as the 'IOUArray' instance, with @IO@ replaced by @ST@ everywhere.
instance MArray (STUArray s) e (ST s) => MArray (STUArray s) (Wrapped e) (ST s) where
  getBounds       = coerceArr (STArr @e Co :->               LiftST Id)            getBounds
  getNumElements  = coerceArr (STArr @e Co :->               LiftST Id)            getNumElements
  newArray        = coerceArr (Id          :-> Co :->        LiftST (STArr @e Co)) newArray
  newArray_       = coerceArr (Id          :->               LiftST (STArr @e Co)) newArray_
  unsafeNewArray_ = coerceArr (Id          :->               LiftST (STArr @e Co)) unsafeNewArray_
  unsafeRead      = coerceArr (STArr @e Co :-> Id :->        LiftST Co)            unsafeRead
  unsafeWrite     = coerceArr (STArr @e Co :-> Id :-> Co :-> LiftST Id)            unsafeWrite

  {-# INLINE getBounds       #-}
  {-# INLINE getNumElements  #-}
  {-# INLINE newArray        #-}
  {-# INLINE newArray_       #-}
  {-# INLINE unsafeNewArray_ #-}
  {-# INLINE unsafeRead      #-}
  {-# INLINE unsafeWrite     #-}

-- |Unboxed mutable 'IOUArray's of 'Wrapped' values.  This instance is the same
-- as the 'STUArray' instance, with @ST@ replaced by @IO@ everywhere.
instance MArray IOUArray e IO => MArray IOUArray (Wrapped e) IO where
  getBounds       = coerceArr (IOArr @e Co :->               LiftIO Id)            getBounds
  getNumElements  = coerceArr (IOArr @e Co :->               LiftIO Id)            getNumElements
  newArray        = coerceArr (Id          :-> Co :->        LiftIO (IOArr @e Co)) newArray
  newArray_       = coerceArr (Id          :->               LiftIO (IOArr @e Co)) newArray_
  unsafeNewArray_ = coerceArr (Id          :->               LiftIO (IOArr @e Co)) unsafeNewArray_
  unsafeRead      = coerceArr (IOArr @e Co :-> Id :->        LiftIO Co)            unsafeRead
  unsafeWrite     = coerceArr (IOArr @e Co :-> Id :-> Co :-> LiftIO Id)            unsafeWrite

  {-# INLINE getBounds       #-}
  {-# INLINE getNumElements  #-}
  {-# INLINE newArray        #-}
  {-# INLINE newArray_       #-}
  {-# INLINE unsafeNewArray_ #-}
  {-# INLINE unsafeRead      #-}
  {-# INLINE unsafeWrite     #-}
